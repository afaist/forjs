//Скрипты для работы с куки
function existCookie (CookieName)
{//Узнает, есть ли куки с данным именем
    return (document.cookie.split(CookieName + '=').length > 1);
}

function CookieValue(CookieName)
{//выдает значение куки с данным именем
    var razrez = document.cookie.split(CookieName + '=');
    if(razrez.length > 1)
    {//куки существуют
        var hvost = razrez[1],
            tzpt = hvost.indexOf(';'),
            EndOfValue = (tzpt - 1) ? tzpt : hvost.length;
        return unescape(hvost.substring(0, EndOfValue));
    }
}

function setCookie(name, value, exp, pth, dmn, sec)
{//Создаем куки с заданными параметрами(название, значение, дата истечения срока, путь, домен, шифрование)
    document.cookie = name + '=' + escape(value)
        + ((exp) ? '; expires=' + exp : '')
        + ((pth) ? '; path=' + pth : '')
        + ((dmn) ? '; domain=' + dmn : '')
        + ((sec) ? '; secure' : '');
}

//Для установки срока действия куки
function TimeAfter(d,h,m)
{// Выдает время через d дней h часов m минут
    var now = new Date(),           //Объект Date
        nowMS = now.getTime(),  //текущее время в милисекундах
        newMS = ((d * 24 + h) * 60 + m) * 60 * 1000 + nowMS; //
    now.setTime(newMS);         //новое время в милисекундах
    return now.toGMTString();
}

function deleteCookie(CookieName)
{
    setCookie(CookieName, '', TimeAfter(-1, 0, 0));
}